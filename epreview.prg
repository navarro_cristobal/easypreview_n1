/*
    ==================================================================
    EasyPreview
    ------------------------------------------------------------------
    Authors: J�rgen B�z
             Timm Sodtalbers
    ==================================================================
*/

#INCLUDE "FiveWin.ch"
#IFDEF __HARBOUR__
  #INCLUDE "davinci.ch"
#ENDIF

*-- FUNCTION -----------------------------------------------------------------
* Name........: Main
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Juergen Baez / Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION Main( P1, P2, P3, P4, P5, P6, P7, P8, P9, P10, P11, P12, P13, P14, P15 )

   LOCAL aFiles, cPath, cDocument, cOrient, cPrinter, oDevice, i, cTempFile
   LOCAL nIndex   := 1
   LOCAL cPrevIni := ""
   LOCAL aMeta    := {}

   IF P1  <> NIL ; cPrevIni += P1  + " " ; ENDIF
   IF P2  <> NIL ; cPrevIni += P2  + " " ; ENDIF
   IF P3  <> NIL ; cPrevIni += P3  + " " ; ENDIF
   IF P4  <> NIL ; cPrevIni += P4  + " " ; ENDIF
   IF P5  <> NIL ; cPrevIni += P5  + " " ; ENDIF
   IF P6  <> NIL ; cPrevIni += P6  + " " ; ENDIF
   IF P7  <> NIL ; cPrevIni += P7  + " " ; ENDIF
   IF P8  <> NIL ; cPrevIni += P8  + " " ; ENDIF
   IF P9  <> NIL ; cPrevIni += P9  + " " ; ENDIF
   IF P10 <> NIL ; cPrevIni += P10 + " " ; ENDIF
   IF P11 <> NIL ; cPrevIni += P11 + " " ; ENDIF
   IF P12 <> NIL ; cPrevIni += P12 + " " ; ENDIF
   IF P13 <> NIL ; cPrevIni += P13 + " " ; ENDIF
   IF P14 <> NIL ; cPrevIni += P14 + " " ; ENDIF
   IF P15 <> NIL ; cPrevIni += P15 + " " ; ENDIF

   cPrevIni := STRTRAN( ALLTRIM( cPrevIni ), '"' )

   IF FILE( cPrevIni ) = .F.
      MsgStop( "Wrong program start." + cPrevIni )
      RETURN(NIL)
   ENDIF

   //SetResDebug(.T.)
   //FERASE( "res.log" )

   cPath     := GetPvProfString( "General", "Path"       , "", cPrevIni )
   cDocument := GetPvProfString( "General", "Document"   , "", cPrevIni )
   cPrinter  := GetPvProfString( "General", "Printer"    , "", cPrevIni )

   oDevice   := PrintBegin( cDocument,, .F., cPrinter )

   aFiles    := DIRECTORY( cPath + "\tmp*.*mf", "D" )

   DO WHILE .T.
      cTempFile := GetPvProfString( "TempFiles", AllTrim(Str( nIndex++ )), "", cPrevIni )
      IF Empty( cTempFile )
         EXIT
      ELSE
         AADD( aMeta, cTempFile )
      ENDIF
   ENDDO

   //FOR i := 1 TO LEN( aFiles )
   //   AADD( aMeta, cPath+ "\"+ aFiles[i,1] )
   //NEXT

   oDevice:aMeta := aMeta

   EasyPreview( oDevice, cPrinter, cPrevIni )

   ResAllFree()
   //CheckRes()

RETURN NIL