/** @package 

        wpdf.c
        
        Copyright() Formglas Neon GmbH 2000
        
        Author: JUERGEN BAEZ
        Created: JB  07.08.2003 12:04:16
	Last change: JB 1.12.2003 16:39:16
*/
#include <Windows.h>
#include "hbapi.h"
#include "wpdf.h"
#include "hbvm.h"

PHB_ITEM itmMsgCallBack;

HINSTANCE PdfEngine; // DLL Handle
DEF_WPDF_ENGINE_PTR; // Macro from wpdf.h to create function pointers


/* ####################################################################### */

void _stdcall wpdfcall_Message(WPDFEnviroment PdfEnv, int number, char *buffer, int bufsize)
{
unsigned long bRet = 0;
PHB_ITEM pReturn ;
PHB_ITEM pnumber = hb_itemPutNI(0, number);
PHB_ITEM pbuffer = hb_itemPutC(0, buffer);

if ( itmMsgCallBack )
   {
    pReturn = hb_vmEvalBlockV( itmMsgCallBack, 2,pnumber, pbuffer);
    //bRet =
    hb_itemGetL( pReturn ) ;
   }
   else bRet= 0;//NULL ;
   hb_itemRelease( pbuffer );
   //return bRet;
   }

//-----------------------------------------------------------------//

HB_FUNC(WPDFCALLBACKMESSAGE)
{
itmMsgCallBack = hb_itemParam( 1 );
if ( !itmMsgCallBack || ( hb_itemType(itmMsgCallBack) != HB_IT_BLOCK ) )
   {
    hb_retl( FALSE );
    return;
   }
else hb_retl( TRUE );
}

//--------------------------------------------------------------------//

HB_FUNC(WPDFINITEX)
{
WPDFEnviroment pdf;
WPDFInfoRecord Info;

Info.SizeOfStruct = sizeof(WPDFInfoRecord);
Info.Encoding        = hb_parni(1);
Info.Compression     = hb_parni(2);
Info.BitCompression  = hb_parni(3);
Info.ThumbNails      = hb_parni(4);
Info.FontMode        = hb_parni(5);       // UseFonts
Info.PageMode        = hb_parni(6);
Info.InputFileMode   = hb_parni(7);
Info.JPEGCompress    = hb_parni(8) ;   //  0=off, - = value, 1..5 for 10, 25, 50, 75, 100
Info.EnhancedOptions = hb_parni(9);
Info.PDFXRes = 72;
Info.PDFYRes = 72;
Info.PDFWidth  = 0; // Default PDF Size
Info.PDFHeight = 0;  //Default DIN A4

// Read PDF Filename
// strcpy(Info.InputPDFFile,"");
// Several Callback functions for Stream Output
Info.OnStreamOpen  = NULL; // wpdfcall_StreamOpen;
Info.OnStreamWrite = NULL; // wpdfcall_StreamWrite;
Info.OnStreamClose = NULL; // wpdfcall_StreamClose;
Info.OnError   = NULL;// wpdfcall_ErrMessage;
Info.OnMessage = wpdfcall_Message;

// Mailmerge, etc
//strcpy(Info.MailMergeStart,""); //@@
//Info.OnGetText = wpdfcall_GetText;
//wpdfcall_GetText, // OnGetText callback

Info.Permission = 0;
strcpy(Info.UserPassword,"");
strcpy(Info.OwnerPassword,"");

// INFO Strings
strcpy(Info.Date,hb_parc(10));
strcpy(Info.ModDate,hb_parc(11));
strcpy(Info.Author,hb_parc(12));
strcpy(Info.Producer,hb_parc(13));
strcpy(Info.Title,hb_parc(14));
strcpy(Info.Subject,hb_parc(15));
strcpy(Info.Keywords,hb_parc(16));

// Reserved, must be 0
Info.Reserved = 0;
pdf = wpdfInitializeEx(&Info);
hb_retni(pdf);
}

//------------------------------------------//

HB_FUNC(WPDFFINALIZE)
{
 wpdfFinalize(hb_parni(1));
 if(PdfEngine!=0)
   {
    FreeLibrary(PdfEngine);
    PdfEngine = 0;
    wpdfInitialize=NULL;
    wpdfFinalize=NULL;
    wpdfFinalizeAll=NULL;
   }
}
//------------------------------------------//

HB_FUNC(WPDFDLLLOAD)
{
char *PDF_LIC_NAME  = hb_parc(2)  ; // License information (empty for DEMO!)
char *PDF_LIC_KEY   = hb_parc(3)  ;
INT   PDF_LIC_CODE =  hb_parni(4) ;
INT   ret ;

ret = LOAD_WPDF_ENGINE(PdfEngine,hb_parc(1)); // 0 geladen 1 nicht 2 Version

if (wpdfSetLicenseKey)
    {
    //MessageBox( 0, "lizensiert", "Fehler", 0 ),
    wpdfSetLicenseKey(PDF_LIC_CODE,PDF_LIC_NAME,PDF_LIC_KEY);
    }
   else MessageBox( 0, "Nicht lizensiert", "Fehler", 0 );

hb_retni(ret);
}

//------------------------------------------//

HB_FUNC(WPDFBEGINDOC)
{
wpdfBeginDoc(hb_parni(1),hb_parc(2),hb_parni(3));
}

//------------------------------------------//
HB_FUNC(WPDFENDDOC)
{
 wpdfEndDoc(hb_parni(1));
}

//------------------------------------------//
HB_FUNC(WPDFSTARTPAGEEX)
{
INT pdf      = hb_parni(1);
INT Width    = hb_parni(2);
INT Height   = hb_parni(3);
INT Rotation = hb_parni(4); // 0 portrait 90 for landscape
wpdfStartPageEx(pdf,Width,Height,Rotation);
}
//------------------------------------------//
HB_FUNC(WPDFSTARTPAGE)
{
INT pdf      = hb_parni(1);
wpdfStartPage(pdf);
}

//------------------------------------------//
HB_FUNC(WPDFENDPAGE)
{
 wpdfEndPage(hb_parni(1));
}

//------------------------------------------//

HB_FUNC(WPDFDC)
{
HDC phdc ;
phdc = wpdfDC(hb_parni(1));
hb_retnl( (LONG) phdc);
}

//------------------------------------------//
HB_FUNC(WPDFDRAWMETAFILE)
{
wpdfDrawMetafile(hb_parni(1),hb_parni(2),hb_parni(3),hb_parni(4),hb_parni(5),hb_parni(6));
}

//-----------------------------------------//
HB_FUNC(WPDFSETSPROP)
{
 wpdfSetSProp(hb_parni(1),hb_parni(2),hb_parc(3));//PdfEnv, int id, char *Value;
}
//------------------------------------------//

HB_FUNC(WPDFSETIPROP)
{
 wpdfSetIProp(hb_parni(1),hb_parni(2),hb_parni(3)); //PdfEnv, int id, int Value);
}
//------------------ Ende inline code ------------------------//

