/*
    ==================================================================
    EasyPreview
    ------------------------------------------------------------------
    Authors: J�rgen B�z
             Timm Sodtalbers
    ==================================================================
*/

#INCLUDE "FiveWin.ch"
#IFDEF __HARBOUR__
  #INCLUDE "davinci.ch"
#ENDIF

*-- FUNCTION -----------------------------------------------------------------
* Name........: Main
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Juergen Baez / Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION Main()

   LOCAL aFiles
   LOCAL cPrevIni  := GetWinDir() + "\EPREVIEW.TMP"
   LOCAL cPath     := GetPvProfString( "General", "Path"       , "", cPrevIni )
   LOCAL cDocument := GetPvProfString( "General", "Document"   , "", cPrevIni )
   LOCAL cOrient   := GetPvProfString( "General", "Orientation", "", cPrevIni )
   LOCAL cPrinter  := GetPvProfString( "General", "Printer"    , "", cPrevIni )
   LOCAL aMeta     := {}

   aFiles    := DIRECTORY(cPath + "\tmp*.*mf", "D" )
   FOR i := 1 TO LEN( aFiles )
       AADD( aMeta, cPath+ "\"+ aFiles[i,1] )
   NEXT

   oDevice := PrintBegin( cDocument,, .F., cPrinter )

   msginfo(cOrient)
   IF VAL( cOrient ) = 2
      oDevice:SetLandscape()
   ELSE
      oDevice:SetPortrait()
   ENDIF

   RPreview( oDevice )

   oDevice:End()

RETURN NIL