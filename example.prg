
#INCLUDE "FiveWin.ch"
#INCLUDE "VRD.ch"
#INCLUDE "TSButton.ch"

STATIC nReport := 1

STATIC oWnd, aExample, cExample, aOutFiles, aFont[3]

*-- FUNCTION -----------------------------------------------------------------
* Name........: Start
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION Start()

   LOCAL oBmp, oBrush

   SET DELETED ON
   SET CONFIRM ON
   SET 3DLOOK ON
   SET MULTIPLE OFF
   SET DATE FORMAT TO "dd.mm.yyyy"
   SET EPOCH TO 1960

   SetHandleCount(100)

   EP_TidyUp()
   EP_SetPath( ".\EPFILES" )
   //EP_SetIniPath( "C:\HARBOUR\EPREVIEW\HELP" )
   //EP_SetIniFile( "timm.ts" )
   //EP_SetDemoMode()
   //EP_SetLanguageFile( "C:\TESTEPREVIEW\TEST.DBF" )
   //EP_SetTempFile( "C:\Dokumente und Einstellungen\test.txt" )
   //EP_DirectMail()

   SET HELPFILE TO ".\HELP\EPREVIEW.HLP"

   DEFINE BRUSH oBrush RESOURCE "BRUSH"

   DEFINE WINDOW oWnd FROM 2, 3 TO 28, 85 ;
      TITLE "EasyPreview - Example application" ;
      BRUSH oBrush

   @ 20, 20 BITMAP oBmp RESOURCE "LOGO" OF oWnd PIXEL

   SET MESSAGE OF oWnd TO "by J�rgen B�z + Timm Sodtalbers - www.reportdesigner.info" ;
      CENTERED KEYBOARD DATE

   ACTIVATE WINDOW oWnd MAXIMIZED ON INIT StartDialog()

   oBrush:End()
   oBmp:End()
   AEVAL( aFont, {|x| x:End() } )

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: StartDialog
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION StartDialog()

   LOCAL oDlg, oRad1, aBtn[2]
   LOCAL cText1 := CRLF + ;
                   "   Welcome to EasyPreview!" + CRLF
   LOCAL cText2 := "    Please let us know if you have any questions or suggestions."
   LOCAL cText3 := CHR(9) + CHR(9) + CHR(9) + CHR(9) + "     J�rgen B�z + Timm Sodtalbers" + CRLF + ;
                   CHR(9) + CHR(9) + CHR(9) + CHR(9) + "     www.reportdesigner.info"

   DEFINE FONT aFont[1] NAME "ARIAL" SIZE 0,-12
   DEFINE FONT aFont[2] NAME "ARIAL" SIZE 0,-14
   DEFINE FONT aFont[3] NAME "ARIAL" SIZE 0,-18 BOLD ITALIC

   DEFINE DIALOG oDlg NAME "EXAMPLE"

   REDEFINE BUTTON ID 101 OF oDlg ACTION ( oDlg:End(), oWnd:End() )
   //REDEFINE SBUTTON ID 101 OF oDlg ACTION ( oDlg:End(), oWnd:End() )

   REDEFINE SAY PROMPT cText1 ID 201 OF oDlg FONT aFont[3] COLOR RGB( 255, 255, 255 ), RGB( 94, 129, 165 )
   REDEFINE SAY PROMPT cText2 ID 202 OF oDlg FONT aFont[2] COLOR RGB( 255, 255, 255 ), RGB( 94, 129, 165 )
   REDEFINE SAY PROMPT cText3 ID 203 OF oDlg FONT aFont[2] COLOR RGB( 255, 255, 255 ), RGB( 94, 129, 165 )

   REDEFINE RADIO oRad1 VAR nReport ID 401, 402, 403, 404, 405 OF oDlg

   REDEFINE BUTTON aBtn[2] PROMPT "Pre&view"ID 103 OF oDlg ACTION PrintChoice( .T. )
   REDEFINE BUTTON aBtn[1] PROMPT "&Print"  ID 102 OF oDlg ACTION PrintChoice()

   REDEFINE BUTTON ID 111 OF oDlg ACTION OpenHtmlHelp()

   //REDEFINE SBUTTON aBtn[2] RESOURCE "B_PREVIEW" PROMPT "Pre&view" FONT aFont[1] ID 103 OF oDlg ACTION PrintChoice( .T. )
   //REDEFINE SBUTTON aBtn[1] RESOURCE "B_PRINT"   PROMPT "&Print"   FONT aFont[1] ID 102 OF oDlg ACTION PrintChoice()

   //REDEFINE SBUTTON ID 111 OF oDlg ACTION OpenHtmlHelp()

   REDEFINE SAY ID 171 OF oDlg COLOR 0, oDlg:nClrPane

   AEVAL( aBtn, { | oBtn | oBtn:SetFont( aFont[1] ) } )

   ACTIVATE DIALOG oDlg CENTERED NOMODAL

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: PrintChoice
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION PrintChoice( lPreview )

   DEFAULT lPreview := .F.

   DO CASE
   CASE nReport = 1
      PrintOrder( lPreview )
   CASE nReport = 2
      PrintTextFeatures( lPreview )
   CASE nReport = 3
      PrintReport( lPreview )
   CASE nReport = 4
      PrintRep2( lPreview, .F. )
   CASE nReport = 5
      PrintRep2( lPreview, .T. )
   CASE nReport = 6
      PrintTest( lPreview )
   ENDCASE

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
*        Name: PrintOrder
* Description: EasyReport example
*       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION PrintOrder( lPreview )

   LOCAL nTotal, nVAT, oVRD
   LOCAL cBesteller :=  ""
   LOCAL nAdrTab1   :=  2.3
   LOCAL nZeile1    :=  5.5
   LOCAL cKunNr     := "12345"
   LOCAL cOrderNr   := "Nr. 54321  Date: 1.9.2004"
   LOCAL cAddress1  := "Sodtalbers+Partner"
   LOCAL cAddress2  := "z.H.: Timm Sodtalbers"
   LOCAL cAddress3  := "Plaggefelder Str. 42"
   LOCAL cAddress4  := "26632 Ihlow"
   LOCAL cCompany   := "Sodtalbers+Partner" + CRLF + "Plaggefelder Str. 42" + CRLF + "26632 Ihlow"

   //EP_DirectPrint()
   //Open report
   //EP_DirectSave()
   //EP_SetUpFile( "c:\test.ini" )
   //EP_SetPDFLicense( "cPDFLicName", "cPDFLicCode", "cPDFLicNr" )

   EASYREPORT oVRD NAME ".\examples\EasyReport Example1.vrd" ;
              PREVIEW lPreview OF oWnd PRINTDIALOG IIF( lPreview, .F., .T. ) ;
              SHOWINFO .F.

   IF oVRD:lDialogCancel = .T.
      RETURN( .F. )
   ENDIF

   //Set and delete expressions
   oVRD:SetExpression( "Page number", "'page: ' + ALLTRIM(STR( oPrn:nPage, 3 ))", ;
                       "Print the current page number" )
   oVRD:SetExpression( "Report name", "oVRD:cReportName", "The Report title" )
   oVRD:DelExpression( "Test" )

   //Print order title
   PRINTAREA 1 OF oVRD ;
      ITEMIDS    { 102, 103, 104, 105, 202, 301, 150 } ;
      ITEMVALUES { cAddress1, cAddress2, cAddress3, cAddress4, cOrderNr, NIL, "55052" }

   //Print position header
   PRINTAREA 2 OF oVRD

   //Print order positions
   USE .\EXAMPLES\EXAMPLE
   nTotal := 0

   DO WHILE .NOT. EOF()

      PRINTAREA 3 OF oVRD ;
         ITEMIDS    { 104, 105, 106, 107 } ;
         ITEMVALUES { EXAMPLE->UNIT                               , ;
                      ALLTRIM(STR( EXAMPLE->PRICE, 10, 2 ))       , ;
                      ALLTRIM(STR( EXAMPLE->TOTALPRICE, 11, 2 ))  , ;
                      ALLTRIM( ".\EXAMPLES\" + EXAMPLE->IMAGE ) }

      EXAMPLE->(DBSKIP())

      nTotal += EXAMPLE->TOTALPRICE

      //New Page
      IF oVRD:nNextRow > oVRD:nPageBreak

         //Print order footer
         PRINTAREA 5 OF oVRD ;
            ITEMIDS    { 101, 102 } ;
            ITEMVALUES { cCompany, ;
                         MEMOREAD( VRD_LF2SF(".\examples\EasyReport Example.txt" ) ) }

         PAGEBREAK oVRD

         //Print position header
         PRINTAREA 2 OF oVRD

      ENDIF

   ENDDO

   EXAMPLE->(DBCLOSEAREA())

   //Print position footer
   nVAT := nTotal * 0.16
   PRINTAREA 4 OF oVRD ;
      ITEMIDS    { 104, 105, 106 } ;
      ITEMVALUES { ALLTRIM(STR( nTotal, 12, 2 ))        , ;
                   ALLTRIM(STR( nVAT, 12, 2 ))          , ;
                   ALLTRIM(STR( nTotal + nVAT , 12, 2 )) }

   //Print order footer on last page
   PRINTAREA 5 OF oVRD ;
      ITEMIDS    { 101, 102 } ;
      ITEMVALUES { cCompany, ;
                   MEMOREAD( VRD_LF2SF(".\examples\EasyReport Example.txt" ) ) }

   //Ends the printout
   END EASYREPORT oVRD

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
*        Name: PrintTextFeatures
* Description: EasyReport example
*       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION PrintTextFeatures( lPreview )

   LOCAL oVRD
   LOCAL cText1 := "Very flexible and easy to built in:" + CRLF + ;
                   "You do not need to learn a new programming or database language. " + ;
                   "The page and print control and the data delivery take place " + ;
                   "directly from your Clipper/Fivewin source code." + CRLF + ;
                   "Instead of oPrn:Say() you will use for example "+ ;
                   "oVrd:PrintItem( oPrn, 1, 101, adress->name ) to print items and " + ;
                   "areas which can be designed by your end users. Already existing " + ;
                   "source code only need some little changes." + CRLF + ;
                   "You don`t get problems with database drivers or runtime moduls." + ;
                   "The classes for the access to the EasyReport data files are delivered " + ;
                   "with source code."
   LOCAL cText2 := "Your customers can use the visual designer without time-consuming " + ;
                   "training. The self explaining interface makes it easy for the user " + ;
                   "to navigate through the program. The properties of the different " + ;
                   "items can be edit in clearly arranged dialogs."

   EASYREPORT oVRD NAME ".\examples\EasyReport Example2.vrd" ;
              PREVIEW lPreview OF oWnd PRINTDIALOG IIF( lPreview, .F., .T. ) ;
              SHOWINFO .F.

   IF oVRD:lDialogCancel = .T.
      RETURN( .F. )
   ENDIF

   PRINTAREA 1 OF oVRD ;
      ITEMIDS    { 101, 102 } ;
      ITEMVALUES { cText1, cText2 }

   END EASYREPORT oVRD

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
*        Name: PrintReport
* Description: EasyReport example
*       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION PrintReport( lPreview )

   LOCAL oVRD

   //Open report
   EASYREPORT oVRD NAME ".\examples\EasyReport Example3.vrd" ;
      PREVIEW lPreview OF oWnd PRINTDIALOG IIF( lPreview, .F., .T. ) SHOWINFO .F.

   IF oVRD:lDialogCancel = .T.
      RETURN( .F. )
   ENDIF

   USE .\EXAMPLES\EXAMPLE3

   oVRD:AreaStart( 1 )
   oVRD:PrintArea( 1 )

   DO WHILE .NOT. EOF()

      oVRD:AreaStart( 2 )
      oVRD:PrintArea( 2 )

      EXAMPLE3->(DBSKIP())

      //New Page
      IF oVRD:nNextRow > oVRD:nPageBreak

         oVRD:PageBreak()

         //Print header
         oVRD:AreaStart( 1 )
         oVRD:PrintArea( 1 )

      ENDIF

   ENDDO

   //Print footer
   oVRD:AreaStart( 3 )
   oVRD:PrintArea( 3 )

   EXAMPLE3->(DBCLOSEAREA())

   //End the printout
   oVRD:End()

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
*        Name: PrintRep2
* Description: Report with groups
*       Autor: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION PrintRep2( lPreview, lBreakAfterGroup )

   LOCAL oVRD, oInfo
   LOCAL nTotalPages := 0

   // This example shows the possibilities of the command ER ONLINE and
   // the check clause. With the check clause you get the total number of
   // pages. The End() method of the VRD class returns an object (oInfo).

   IF lBreakAfterGroup = .T.

      ER QUICK oVRD NAME ".\examples\EasyReport Example4.vrd" ;
                    PREVIEW lPreview                          ;
                    OF oWnd                                   ;
                    PRINTDIALOG IIF( lPreview, .F., .T. )     ;
                    CHECK .T.                                 ;
                    ACTION ReportIt( oVRD, lBreakAfterGroup )

   ELSE

      EASYREPORT oVRD NAME ".\examples\EasyReport Example4.vrd" CHECK .T. OF oWnd

      ReportIt( oVRD, lBreakAfterGroup )

      IF oVRD:lDialogCancel = .T.
         RETURN( .F. )
      ENDIF

      oInfo := oVRD:End()

      EASYREPORT oVRD NAME ".\examples\EasyReport Example4.vrd" ;
         PREVIEW lPreview OF oWnd PRINTDIALOG IIF( lPreview, .F., .T. ) SHOWINFO .F.

      IF oVRD:lDialogCancel = .T.
         RETURN( .F. )
      ENDIF

      oVRD:Cargo := ALLTRIM(STR( oInfo:nPages ))
      oVRD:oInfo := oInfo

      ReportIt( oVRD, lBreakAfterGroup )

      END EASYREPORT oVRD

   ENDIF

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: ReportIt
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION ReportIt( oVRD, lBreakAfterGroup )

   LOCAL cCurState
   LOCAL nGroupTotal := 0
   LOCAL nTotal      := 0

   DELETE FILE ".\EXAMPLES\EXAMPLE4.CDX"
   USE .\EXAMPLES\EXAMPLE4
   INDEX ON EXAMPLE4->STATE TAG STATE
   DBGOTOP()

   cCurState := EXAMPLE4->STATE
   PRINTAREA 1 OF oVRD

   DO WHILE .NOT. EOF()

      oVRD:AreaStart( 2, .T. )

      nGroupTotal += EXAMPLE4->SALARY
      nTotal      += EXAMPLE4->SALARY

      EXAMPLE4->(DBSKIP())

      //Group
      IF EXAMPLE4->STATE <> cCurState

         PRINTAREA 3 OF oVRD ;
            ITEMIDS    { 101, 102 } ;
            ITEMVALUES { "Total for state " + ALLTRIM( cCurState ) + ":", ;
                         ALLTRIM(STR( nGroupTotal, 12, 2 )) }

         cCurState   := EXAMPLE4->STATE
         nGroupTotal := 0

         //Total
         PRINTAREA 4 OF oVRD ;
            ITEMIDS    { 101 } ;
            ITEMVALUES { ALLTRIM(STR( nTotal, 12, 2 )) }

         IF lBreakAfterGroup = .T.
            PRINTAREA 1 OF oVRD PAGEBREAK
         ENDIF

      ENDIF

      //Page break if necessary
      IF oVRD:nNextRow > oVRD:nPageBreak
         PRINTAREA 1 OF oVRD PAGEBREAK
      ENDIF

   ENDDO

   EXAMPLE4->(DBCLOSEAREA())

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: PrintTest
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION PrintTest( lPreview )

   local oPrn, oFont
   local nRowStep, nColStep
   local nRow := 0, nCol := 0, n, m

   //PrnSetSize( 2100, 1200 )     //To adjust a different printer paper size!

   PRINT oPrn NAME "Testing the printer object from FiveWin" PREVIEW

      oPrn:SetSize( 2100, 1200 )
      //oPrn:SetLandscape()
      //oPrn:SetPage(9)

      DEFINE FONT oFont NAME "Ms Sans Serif" SIZE 0, -20 OF oPrn

      nRowStep = oPrn:nVertRes() / 20   // We want 20 rows
      nColStep = oPrn:nHorzRes() / 15   // We want 15 cols

      PAGE
         for n = 1 to 20  // rows
             nCol = 0
             oPrn:Say( nRow, nCol, Str( n, 2 ) )
             nCol += nColStep
             for m = 1 to 15
                oPrn:Say( nRow, nCol, "+" )
                nCol += nColStep
             next
             nRow += nRowStep
         next
         oPrn:Line( 0, 0, nRow, nCol )
      ENDPAGE
      PAGE
         for n = 1 to 20  // rows
             nCol = 0
             oPrn:Say( nRow, nCol, Str( n, 2 ) )
             nCol += nColStep
             for m = 1 to 15
                oPrn:Say( nRow, nCol, "+" )
                nCol += nColStep
             next
             nRow += nRowStep
         next
         oPrn:Line( 0, 0, nRow, nCol )
      ENDPAGE

   ENDPRINT

   oFont:End()      // Destroy the font object

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: OpenHtmlHelp
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Juergen Baez / Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION OpenHtmlHelp( cTopicID, nTopicNr, cHelpFile )

   LOCAL cPara := ""

   DEFAULT cHelpFile := ".\help\epreview.chm"

   IF nTopicNr <> NIL
      cPara := "-mapid " + ALLTRIM(STR( nTopicNr, 10 )) + " " + ALLTRIM( cHelpFile )
   ELSEIF cTopicID <> NIL
      cPara := '"' + ALLTRIM( cHelpFile ) + "::/" + ALLTRIM( cTopicID ) + '"'
   ELSE
      cPara := '"' + ALLTRIM( cHelpFile ) + '"'
   ENDIF

   ShellExecute( 0, "Open", "HH.exe", ALLTRIM( cPara ), NIL, 1 )

RETURN (.T.)


*-- FUNCTION -----------------------------------------------------------------
* Name........: GetSysFont
* Beschreibung:
* Argumente...: None
* R�ckgabewert: .T.
* Author......: Timm Sodtalbers
*-----------------------------------------------------------------------------
FUNCTION GetSysFont()

RETURN "Ms Sans Serif"