/** @package

        mypdf.prg
        Copyright() Formglas Neon GmbH 2002

        Author: JUERGEN BAEZ
        Created: JB  08.06.2002 17:09:23
	Last change: JB 10.03.2006 13:39:54
*/

#include "FiveWin.ch"

CLASS TPDF

DATA cINI            AS CHARACTER INIT ""
DATA cDocument       AS CHARACTER INIT ""
DATA oPrn            AS OBJECT
DATA idPDF           AS NUMERIC INIT 0
DATA hdc             AS NUMERIC
DATA nwidth          AS NUMERIC INIT 0
DATA nheight         AS NUMERIC INIT 0
DATA bBlockmsg       AS NUMERIC INIT {||"NULL"}


DATA nEncoding        AS NUMERIC INIT 0   // 0 = none, 1=ASCII85, 2=Hex
DATA nCompression     AS NUMERIC INIT 3   // 0=off, 1=deflate, 2=runlength, 3=fast
DATA nBitCompression  AS NUMERIC INIT 0   // 0=auto, 1=deflate, 2=jpeg
DATA nThumbNails      AS NUMERIC INIT 1   // 0=none, 1=color, 2=monchrome
DATA nFontMode        AS NUMERIC INIT 0   // UseFonts
DATA nPageMode        AS NUMERIC INIT 1
DATA nInputFileMode   AS NUMERIC INIT 3
DATA nJPEGCompress    AS NUMERIC INIT 0    //  0=off, - = value, 1..5 for 10, 25, 50, 75, 100
DATA nEnhancedOptions AS NUMERIC INIT 0
// JB START//
DATA cDate            AS CHARACTER INIT ""
DATA cModDate         AS CHARACTER INIT ""
DATA cAuthor          AS CHARACTER INIT ""
DATA cProducer        AS CHARACTER INIT ""
DATA cTitle           AS CHARACTER INIT ""
DATA cSubject         AS CHARACTER INIT ""
DATA cKeywords        AS CHARACTER INIT ""
// JB ENDE//
DATA nPDFXRes         AS NUMERIC INIT 72          // PDF Resolution
DATA nPDFYRes         AS NUMERIC INIT 72          // PDF Resolution
DATA nPDFWidth        AS NUMERIC INIT 0   //Default PDF Size
DATA nPDFHeight       AS NUMERIC INIT 0   //Default DIN A4

DATA cDllFile   AS CHARACTER
DATA cLIC_NAME  AS CHARACTER
DATA cLIC_KEY   AS CHARACTER
DATA nLIC_CODE  AS NUMERIC


METHOD New( cIniFile ) CONSTRUCTOR
METHOD PDFInit()
METHOD StartDoc()
METHOD EndDoc  ()    INLINE wpdfEndDoc(::idPDF)

METHOD StartPage()
METHOD EndPage()     INLINE wpdfEndPage(::idPDF)

METHOD regcallbackmsg(lyesno)
METHOD msgcallback(nr,ctext)
METHOD DrawMetafile(henhmeta,cMetafile)
METHOD End()         INLINE wpdfFinalize(::idPDF)

ENDCLASS

//------------------ Methoden ------------------------//

METHOD new( cinifile, cDocument, oPrn, cPDFLicName, cPDFLicCode, nPDFLicNr ) CLASS TPDF

::cIni             := cinifile
::cDocument        := cDocument
::oPrn             := oPrn
::nEncoding        :=  VAL( GetPvProfString( "PDF", "Encoding", "0", ::cIni ) )
::nCompression     :=  VAL( GetPvProfString( "PDF", "Compression", "1", ::cIni ) )
::nBitCompression  :=  VAL( GetPvProfString( "PDF", "BitCompression", "0", ::cIni ) )
::nThumbNails      :=  VAL( GetPvProfString( "PDF", "ThumbNails", "1", ::cIni ) )
::nFontMode        :=  VAL( GetPvProfString( "PDF", "FontMode", "0", ::cIni ) )       // UseFonts
::nPageMode        :=  VAL( GetPvProfString( "PDF", "PageMode", "1", ::cIni ) )
::nInputFileMode   :=  VAL( GetPvProfString( "PDF", "InputFileMode", "0", ::cIni ) )
::nJPEGCompress    :=  VAL( GetPvProfString( "PDF", "JPEGCompress", "0", ::cIni ) )    //  0=off, - = value, 1..5 for 10, 25, 50, 75, 100
::nEnhancedOptions :=  VAL( GetPvProfString( "PDF", "EnhancedOptions", "3", ::cIni ) )
::cDate            :=  DtoC(Date())
::cModDate         :=  ::cDate //DtoC(Date())
// JB START//
::cAuthor          :=  GetPvProfString( "PDF", "Author", "EP", ::cIni )
::cProducer        :=  GetPvProfString( "PDF", "Producer", "EP", ::cIni )
::cTitle           :=  GetPvProfString( "PDF", "Title", "", ::cIni )
::cSubject         :=  GetPvProfString( "PDF", "Subject", "", ::cIni )
::cKeywords        :=  GetPvProfString( "PDF", "Keywords", "", ::cIni )
// JB ENDE //
::nPDFXRes         := 72
::nPDFYRes         := 72
::nPDFWidth        := 0 // Default PDF Size
::nPDFHeight       := 0  //Default DIN A4
::bBlockmsg        :={|nr,ctext|::msgcallback(nr,ctext)}

// JB Start //
::cLIC_NAME   := cPDFLicName  // "Sodtalbers+Partner"
::cLIC_KEY    := cPDFLicCode  // "C1yPNce_earEr8deafka"
::nLIC_CODE   := nPDFLicNr    // 686041
::cDllFile   := "EPPDF.DLL"
// JB Ende //


//::PDFinit()

return (self)


//----------------- DLL laden und Lizensieren-------------------------//

METHOD PDFInit()  CLASS  TPDF

if file(::cDllFile)
   if wpdfdllload( ::cDllFile,::cLIC_NAME,::cLIC_KEY,::nLIC_CODE )<>0
     RETURN .F.
  endif
else
  msgalert(::cDllFile +" nicht gefunden")
  RETURN .F.
endif

RETURN .T.

//------------------Callback Registrieren -----------------------//

METHOD regcallbackmsg(lyesno)  CLASS  TPDF

if lyesno
   wpdfcallbackmessage(::bBlockmsg)
endif

return NIL

//-------------------Start Dukument-----------------------//

METHOD StartDoc() CLASS  TPDF

LOCAL asize
   // JB zusätzliche Daten aufgenommen//
asize := ::oprn:GetPhySize()

::nwidth    := ( aSize[1] /25.4 * ::nPDFXRes)
::nheight   := ( asize[2] /25.4 * ::nPDFYRes)


::idPDF := wpdfinitex(::nEncoding,::nCompression,::nBitCompression,::nThumbNails,::nFontMode, ;
           ::nPageMode,::nInputFileMode,::nJPEGCompress,::nEnhancedOptions,;
           ::cDate,::cModDate,::cAuthor,::cProducer,::cTitle,::cSubject,::cKeywords )
wpdfBeginDoc(::idPDF,::cDocument,0)

return NIL
//------------------------------------------//

METHOD StartPage() CLASS  TPDF

local nlandscape := 0
local asize := array()

*asize := ::oprn:GetPhySize()
*::nwidth    := ( aSize[1] /25.4 * ::nPDFXRes)
*::nheight   := ( asize[2] /25.4 * ::nPDFYRes)


//------------- Ende neu ----------------------//
*::nwidth    := (::oprn:nHorzSize() /25.4 * ::nPDFXRes)
*::nheight   := (::oprn:nVertSize() /25.4 * ::nPDFYRes)

wpdfStartPageEx(::idPDF,::nwidth,::nheight,nlandscape)
::hdc   := wpdfdc(::idPDF)

RETURN  NIL

//------------------  ------------------------//

METHOD  msgcallback (nr,ctext )CLASS  TPDF
ctext :=alltrim(ctext)

do case
    case nr == 2
     *   msginfo("Datei  "+ctext+" gespeichert")
    case nr == 3
      *  msginfo("Schrift "+ctext+" eingebunden")
endcase
return nil

// ----------------------JB Start----------------- //

METHOD drawmetafile(henhmeta,cMetafile) CLASS TPDF

local x := 0
local y := 0
local w := ::nwidth
local h := ::nheight

if henhmeta == NIL
   henhmeta := getenhmetafile(cMetafile)
endif

wpdfdrawmetafile(::idpdf,henhmeta)//,x,y,w,h)

return NIL


//------------------Inline c-code  ------------------------//